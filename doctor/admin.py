from django.contrib import admin
from  .models import Doctor, Comment

admin.site.register(Doctor)
admin.site.register(Comment)