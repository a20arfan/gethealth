from django.shortcuts import render, redirect, get_object_or_404
from .models import Doctor, Comment
from django.contrib import messages
from django.urls import reverse
from django.http import HttpResponseRedirect, JsonResponse

def DetailDoctor(request, id):
	doctor = Doctor.objects.get(id = id)
	context = {
		"data": doctor,
	}
	return render(request, 'doctor/detailDoctor.html', context)

def AddComment(request):
	if request.user.is_authenticated:
		id = request.POST.get('doctorID', None)
		doctor = Doctor.objects.get(id = id)
		comment = request.POST.get('comment', None)
		if (str(comment) != str("")):
			Comment.objects.create(username = request.user.username, doctor = doctor, text = comment)
		return redirect('/doctor/' + id)
	else:
		messages.error(request, "You have to log in first to post a comment.")			
		return redirect('/login')

def MyJsonFunction(request, id):
	doctor = Doctor.objects.get(id = id)
	queryset = Comment.objects.filter(doctor = doctor)
	
	dict = {}
	num = 1
	for comments in queryset:
		dict[num] = [comments.username, comments. text]
		num = num + 1
	
	return JsonResponse(dict)
	

def ListDoctor(request, **kwargs):
	doctors = Doctor.objects.all()
	doctorsList = []
	for i in doctors:
		doctorsList.append(i)
	context = {'doctorsList': doctorsList} 
	return render(request, 'doctor/listDoctor.html', context)

def LikeButton(request, id):
	if request.user.is_authenticated:
		doctor_like = Doctor.objects.get(id=id)
		doctor_like.like.add(request.user)
		return redirect('/doctor/')
	else:
		messages.error(request, "You have to log in first to post a comment.")			
		return redirect('/login')
