from django.db import models
from django.conf import settings

class Doctor(models.Model):
	profile_picture = models.ImageField(upload_to='Profile_Picture/', default='default.jpg')
	name = models.CharField(max_length=50)
	degree_name = models.CharField(max_length=80)
	message = models.TextField()
	known_for = models.TextField()
	path_to = models.TextField()
	passion = models.TextField()
	fav_quote = models.TextField()
	fav_quote_by = models.CharField(max_length=50)
	like = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="doctor_profile") #user-likes-doctor

	def jumlah_total_like(self):
		return self.like.count()

class Comment(models.Model):
	username = models.CharField(max_length=60)
	text = models.TextField(blank = False, default="")
	doctor = models.ForeignKey(Doctor, on_delete=models.CASCADE) #Many to one