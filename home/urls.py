from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('covid_cases/', views.covid_cases, name='covid_cases'),
    path('post_suggestion/', views.post_suggestion, name='post_suggestion'),
    path('process_suggestions/', views.process_suggestions, name='process_suggestions'),
    path('suggestions/', views.list_of_suggestions, name='suggestions'),
]