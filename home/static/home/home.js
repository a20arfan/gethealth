$(document).ready(function() {
	$.ajax({
		url: '/covid_cases',
		success: function(data) {
			var cases = data[19];
			var active = cases.attributes.Active.toString();
			var confirmed = cases.attributes.Confirmed.toString();
			var deaths = cases.attributes.Deaths.toString();
			var recovered = cases.attributes.Recovered.toString();
			var date = new Date(cases.attributes.Last_Update);
			$(".num-confirmed").append(confirmed.substring(0, confirmed.length-3) + "," + confirmed.substring(confirmed.length-3, confirmed.length))
			$(".num.active-cases").append(active.substring(0, active.length-3) + "," + active.substring(active.length-3, active.length))
			$(".num.recovered").append(recovered.substring(0, recovered.length-3) + "," + recovered.substring(recovered.length-3, recovered.length))
			$(".num.deaths").append(deaths.substring(0, deaths.length-3) + "," + deaths.substring(deaths.length-3, deaths.length))
			$(".last-update").append(date)
		}
	})

	$(".suggestion").submit(function (e) {
		var serializedData = $(this).serialize();
		e.preventDefault();
		$.ajax({
			type:'POST',
			url: '/post_suggestion/',
			data: {
				name:$("#id_name").val(),
				subject:$("#id_subject").val(),
				suggestion:$("#id_suggestion").val(),
	            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
	            action: 'post'
			},
			success: function(data) {
				console.log(data.status)
				if (data.status == "success") {					
					$(".submitted").css("display", "block")
				}else{
					window.location.href = "/login";
				}

			}
		})
	})

	$('.wrapper').hover(function() {
			$(this).css("margin-top", "0");
			$(this).css("margin-bottom", "0");
			$(this).css("padding", "10px 20px 10px 20px");
			$(this).find(".num").css("font-size", "40px");
			$(this).find(".text-cases").css("font-size", "26px");
			$(this).find(".text-cases").css("margin-top", "5px");
		},
		function() {
			$(this).css("margin-top", "20px")
			$(this).css("margin-bottom", "20px")
			$(this).css("padding", "0 0 0 0");
			$(this).find(".num").css("font-size", "30px");
			$(this).find(".text-cases").css("font-size", "21px");
			$(this).find(".text-cases").css("margin-top", "-10px");
		});

	$('[data-toggle="tooltip"]').tooltip();

	$("#test > div:gt(0)").hide();

	setInterval(function() { 
	  $('#test > div:first')
	    .fadeOut(1000)
	    .next()
	    .fadeIn(1000)
	    .end()
	    .appendTo('#test');
	},  3000);

	$(".close-submit").click(function() {
		$(".submitted").css("display", "none")
	})
})