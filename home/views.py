from django.shortcuts import render, redirect
from .forms import SuggestionForm
from .models import Suggestions
from article.models import News, TipsAndTrick
from doctor.models import Doctor
from itertools import chain
from operator import attrgetter
from django.contrib import messages
from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.core import serializers
import requests
import json

def index(request):
	form = SuggestionForm()
	news = News.objects.all()
	tnt = TipsAndTrick.objects.all()
	doctors = Doctor.objects.all()
	data = sorted(
       chain(news, tnt),
       key=attrgetter('date'), reverse=True)

	response = {
	'form':form,
	'data':data,
	'news':news,
	'tnt':tnt,
	'doctors':doctors,
	}
	return render(request, 'home/index.html', response)

def post_suggestion(request):
	if request.method=='POST':
		if request.user.is_authenticated:
			form = SuggestionForm(request.POST)
			if form.is_valid():
				data = Suggestions()
				data.name = request.POST.get('name')
				data.subject = request.POST.get('subject')
				data.suggestion = request.POST.get('suggestion')
				saved_data = data.save()
				return JsonResponse({'status':'success'})
		else:
			messages.error(request, "You have to log in first to submit a suggestion.")
			return JsonResponse({'status':'error'})

def covid_cases(request):
	url = "https://api.kawalcorona.com/"
	result = requests.get(url)
	data = json.loads(result.content)
	return JsonResponse(data, safe=False)

def process_suggestions(request):
	suggestions = Suggestions.objects.all()
	serialized = serializers.serialize('json', suggestions)
	return HttpResponse(serialized, content_type="text/json-comment-filtered")

def list_of_suggestions(request):
	if(request.user.is_superuser):
		form = SuggestionForm()
		suggestions = Suggestions.objects.all()
		return render(request, "home/suggestions.html", {'form' : form, 'suggestions' : suggestions})
	else:
		messages.error(request, "Only website admin can access this page.")
		return redirect('/')