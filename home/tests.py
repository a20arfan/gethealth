from django.test import TestCase, Client
from .models import Suggestions
from django.apps import apps
from .apps import HomeConfig
from django.contrib.messages import get_messages
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.urls import resolve
from .views import covid_cases

class TestHome(TestCase):
	def test_if_home_url_exist(self):
		response = Client().get('/')
		self.assertEquals(response.status_code, 200)

	def test_if_home_url_uses_the_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'home/index.html')

	def test_if_suggestions_model_exist(self):
		Suggestions.objects.create(
			name="nama",
			subject='ini subject',
			suggestion="ini suggestion")
		num_of_suggestions = Suggestions.objects.all().count()
		self.assertEquals(num_of_suggestions, 1)

	def test_if_success_message_show_when_submitting_a_suggestion_and_logged_in(self):
		email = 'whatnewgirl@gmail.com'
		username = 'newgirl'
		password = 'isshepretty'
		user = get_user_model().objects.create_user(
			email=email,
			username=username,
			password=password
			)
		klien = Client()
		klien.post('/login/', data={
			"email" : "whatnewgirl@gmail.com",
			"password": "isshepretty"})
		klien.post('/post_suggestion/', {
			'name': 'nama',
			'subject': 'ini subject',
			'suggestion': 'ini suggestion'})
		response = klien.post('/', {
			'name': 'nama',
			'subject': 'ini subject',
			'suggestion': 'ini suggestion'})
		html_response = response.content.decode('utf8')
		self.assertIn('Your suggestion has been submitted! Thank you.', html_response)

	def test_if_success_message_show_when_submitting_a_suggestion_and_not_logged_in(self):
		klien = Client()
		klien.post('/post_suggestion/', {
			'name': 'nama',
			'subject': 'ini subject',
			'suggestion': 'ini suggestion'})
		response = klien.post('/', {
			'name': 'nama',
			'subject': 'ini subject',
			'suggestion': 'ini suggestion'})
		html_response = response.content.decode('utf8')
		self.assertIn('You have to log in first to submit a suggestion.', html_response)


	def test_api_success(self):
		response = Client().get('/process_suggestions/')
		self.assertEqual(response.status_code, 200)

	def test_if_OUR_DOCTORS_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("OUR DOCTORS", html_response)

	def test_if_BOOK_CONSULTATION_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("BOOK CONSULTATION", html_response)

	def test_if_HELP_US_IMPROVE_OUR_PAGE_text_exist_in_index_url(self):
		response = Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("HELP US IMPROVE OUR PAGE", html_response)

	def test_app_name(self):
		self.assertEquals(HomeConfig.name, 'home')
		self.assertEquals(apps.get_app_config('home').name, 'home')

	def test_home_model_str_method(self):
		html_response = Suggestions(name="uvuvevwevwe onyetenyevwe ugwemubwem osas")
		self.assertEqual(str(html_response), "uvuvevwevwe onyetenyevwe ugwemubwem osas")

	def test_login_url_exists(self):
		response = self.client.get('/login/')
		self.assertEqual(response.status_code, 200)
		response = self.client.get('/logout/')
		self.assertEqual(response.status_code, 302)
		response = self.client.get('/register/')
		self.assertEqual(response.status_code, 200)

	def test_if_covid_cases_url_exist(self):
		response = Client().get('/covid_cases/')
		self.assertEquals(response.status_code, 200)

	def test_covid_cases_url_resolves_to_covid_cases_view(self):
		found = resolve('/covid_cases/')
		self.assertEquals(found.func, covid_cases)

	def test_message_when_non_admin_access_suggestions(self):
		email = 'whatnewgirl@gmail.com'
		username = 'newgirl'
		password = 'isshepretty'
		user = get_user_model().objects.create_user(
			email=email,
			username=username,
			password=password
			)
		klien = Client()
		klien.post('/login/', data={
			"email" : "whatnewgirl@gmail.com",
			"password": "isshepretty"})
		klien.get('/suggestions/')
		response = klien.get('/')
		html_response = response.content.decode('utf8')
		self.assertIn("Only website admin can access this page.", html_response)

	def test_if_suggestion_page_shows_when_accessed_by_admin(self):
		user = get_user_model().objects.create_superuser(
			'testadmin@gmail.com',
			'test123',
			'testadmin123'
			)
		klien = Client()
		klien.post('/login/', data={
			"email" : "testadmin@gmail.com",
			"password": "testadmin123"})
		response = klien.get('/suggestions/')
		self.assertTemplateUsed(response, 'home/suggestions.html')