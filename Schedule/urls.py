from django.urls import path
from . import views


app_name = 'Schedule'

urlpatterns = [
    path('', views.index, name='index'),
    path('booking_form/<int:pk>', views.book, name='form'),
    path('my_appointments/', views.appointmentlist, name='appointmentlist'),
    path('schedule_api/', views.hour_list, name='hour_list'),
    path('look_up/', views.obtain_json, name='lookup'),
]