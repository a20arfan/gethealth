from django.shortcuts import render, redirect

# Models
from .models import Day, Hour, Booking
from doctor.models import Doctor
from account.models import Account

# Form
from .forms import BookingForm

# Authentication
from django.contrib.auth import login, authenticate
from django.contrib import messages
from django.http import HttpResponseNotFound, JsonResponse

# Rest Framework and Serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import HourSerializer

# For Json thingy
import requests, json

from django.urls import reverse, resolve

# Method API
@api_view(['GET'])
def hour_list(request):
	hours = Hour.objects.all().filter(available=True).order_by('hour', 'day__doctor__name')
	serializer = HourSerializer(hours, many=True)
	return Response(serializer.data)

@api_view(['DELETE'])
def hour_delete(request, pk):
	hour = Hour.objects.get(id=pk)
	hour.delete()
	return Response(serializer.data)

# method views biasa
def index(request):
	time = Hour.objects.all()
	schedule = {
		"Monday" : Hour.objects.filter(day__name="Monday", available=True).order_by('hour', 'day__doctor__name'),
		"Tuesday" : Hour.objects.filter(day__name="Tuesday", available=True).order_by('hour', 'day__doctor__name'),
		"Wednesday" : Hour.objects.filter(day__name="Wednesday", available=True).order_by('hour', 'day__doctor__name'),
		"Thursday" : Hour.objects.filter(day__name="Thursday", available=True).order_by('hour', 'day__doctor__name'),
		"Friday" : Hour.objects.filter(day__name="Friday", available=True).order_by('hour', 'day__doctor__name'),
	}

	context = {
		"schedule" : schedule,
	}
	
	return render(request, "Schedule/bookingpage.html", context)

def book(request, pk):
	if request.user.is_authenticated:
		hour = Hour.objects.get(id=pk)
		user = Account.objects.get(id=request.user.id)
		form = BookingForm()
		if request.method == 'POST':
			form = BookingForm(request.POST)
			if form.is_valid():
				# orderer = form.cleaned_data['orderer']
				# email = form.cleaned_data['email']
				phone = form.cleaned_data['phone']
				booking = Booking.objects.create(
					orderer = user,
					doctor = hour.day.doctor,
					time = hour,
					email = user.email,
					phone = phone,
				)
				booking.save()
				hour.available = False
				hour.save()
				hour_delete(request, hour.id)
				messages.success(request, "You have successfully booked a schedule. See it in My Appointment.")
				return redirect('Schedule:index')
		context = {
			'form' : form,
			'hour' : hour,
			'customer' : user,
		}
		return render(request, 'Schedule/bookingform.html', context)

	else:
		messages.error(request, "You have to log in first to book a schedule.")
		return redirect('/login')

def appointmentlist(request):
	if request.user.is_authenticated:
		user_appointment = Booking.objects.filter(orderer__id=request.user.id).order_by('time__day__name', "time")
		context = {
			"myappointments" : user_appointment,
		}
		return render(request, "Schedule/myappointments.html", context)
	else:
		return HttpResponseNotFound("Sorry, this page does not exist.")

def obtain_json(request):
	# Fetch data yang diinput
	query = request.GET.get('q', None)

	# Buat ngeget url API nya di server manapun
	absolute_url = request.build_absolute_uri("/schedule/schedule_api/")

	ret = requests.get(absolute_url)
	data = json.loads(ret.content)

	# Disaring dulu datanya, sama diset agar case-insensitive
	output = [x for x in data if query in x["day"]["doctor"]["name"].lower()]

	return JsonResponse(output, safe=False)