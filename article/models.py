from django.db import models
from django.contrib.auth.models import User
from autoslug import AutoSlugField
from tinymce import models as tinymce_models, HTMLField
from account.models import Account


# Create your models here.
class News(models.Model):
    title = models.CharField(max_length=100, default="")
    slug = AutoSlugField(populate_from='title')
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def slugify_function(self, content):
        return content.replace('_', '-').lower()

    def __str__(self):
        return self.title

class TipsAndTrick(models.Model):
    title = models.CharField(max_length=100, default="")
    slug = AutoSlugField(populate_from='title')
    date = models.DateField(auto_now_add=True)
    content = HTMLField('Content')
    image = models.ImageField(null=True, blank=True, upload_to='image')

    def __str__(self):
        return self.title
    
    def slugify_function(self, content):
        return content.replace('_', '-').lower()

class CommentsNews(models.Model):
    post = models.ForeignKey(News,on_delete=models.CASCADE,related_name='comments_news', default="", null=True)
    user = models.ForeignKey(Account, on_delete= models.CASCADE,related_name='news_posts', default="", null=True)
    comment = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['published']

    def __str__(self):
        return 'Comment {} by {}'.format(self.comment, self.user)

class CommentsTipsAndTrick(models.Model):
    post = models.ForeignKey(TipsAndTrick,on_delete=models.CASCADE,related_name='comments_tips_and_trick', default="", null=True)
    user = models.ForeignKey(Account, on_delete= models.CASCADE,related_name='tips_posts', default="", null=True)
    comment = models.TextField()
    published = models.DateTimeField(auto_now_add=True)
    active = models.BooleanField(default=True)

    class Meta:
        ordering = ['published']

    def __str__(self):
        return 'Comment {} by {}'.format(self.comment, self.user)