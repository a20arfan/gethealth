from django.shortcuts import render, get_object_or_404, redirect
from django.http import JsonResponse
from .models import News, TipsAndTrick, CommentsNews, CommentsTipsAndTrick
from .forms import CommentNewsForm, CommentTipsAndTrickForm
from django.core import serializers
from .serializers import NewsSerializer, TipsAndTrickSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response
import json
import requests


# Create your views here.
def article(request):
    news = News.objects.all()
    tips = TipsAndTrick.objects.all()
    context = {
        "news": news,
        "tips": tips
    }
    return render(request, "article.html", context)


def news(request, slug):
    post = get_object_or_404(News, slug=slug)
    post_id = post.id

    user = request.user
    user_id = user.id

    if request.method == 'POST':
        comment_form = CommentNewsForm(data=request.POST)
        if comment_form.is_valid():
            comment_form.instance.post_id = post_id
            comment_form.instance.user_id = user_id
            comment_form.save()
            comment_form = CommentNewsForm()
    else:
        comment_form = CommentNewsForm()

    comments = post.comments_news.filter(active=True)

    context = {
        'post': post,
        'comment_form': comment_form,
        'comments': comments,
        'user': user,
    }
    return render(request, 'news.html', context)


def tipsandtrick(request, slug):
    post = get_object_or_404(TipsAndTrick, slug=slug)
    user = request.user

    if request.method == 'POST':
        comment_form = CommentTipsAndTrickForm(data=request.POST)
        if comment_form.is_valid():
            comment_form.save()
            comment_form = CommentTipsAndTrickForm()
    else:
        comment_form = CommentTipsAndTrickForm()

    comments = post.comments_tips_and_trick.filter(active=True)

    context = {
        'post': post,
        'comment_form': comment_form,
        'comments': comments,
        'user': user,
    }
    return render(request, 'tipsandtrick.html', context)

def search_news(request):
    context = {}
    return render(request, "search_news.html", context)

def search_tips(request):
    context = {}
    return render(request, "search_tips.html", context)

@api_view(['GET'])
def get_news(request):
    if request.method == "GET":
        news = News.objects.all()
        serializer = NewsSerializer(news, many=True)
        return Response(serializer.data)

@api_view(['GET'])
def get_tips(request):
    if request.method == "GET":
        tips = TipsAndTrick.objects.all()
        serializer = TipsAndTrickSerializer(tips, many=True)
        return Response(serializer.data)

def data_news(request):
    query = request.GET.get('q', None)
    absolute_url = request.build_absolute_uri('/article/news_api/')
    ret = requests.get(absolute_url)
    data_news = json.loads(ret.content)
    output_news = [x for x in data_news if query in x['title'].lower()]
    return JsonResponse(output_news, safe=False)

def data_tips(request):
    query = request.GET.get('q', None)
    absolute_url = request.build_absolute_uri('/article/tips_api/')
    ret = requests.get(absolute_url)
    data_tips = json.loads(ret.content)
    output_tips = [x for x in data_tips if query in x['title'].lower()]
    return JsonResponse(output_tips, safe=False)