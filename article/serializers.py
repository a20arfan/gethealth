from rest_framework import serializers
from .models import News, TipsAndTrick

class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        fields = '__all__'

class TipsAndTrickSerializer(serializers.ModelSerializer):
    class Meta:
        model = TipsAndTrick
        fields = '__all__'

