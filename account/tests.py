from django.test import TestCase
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from django.contrib.auth import get_user_model # used to get user in django.auth

from account.models import Account
import account.views
from django.conf import settings


class DetailPageUnit(TestCase):
	def test_account_urls_have_the_right_response_with_no_login(self):
		response = Client().get('/register/')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)
		response = Client().get('/logout/') # After login
		self.assertEqual(response.status_code, 302)
		
	def test_account_urls_have_the_right_html_response(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response,"account/register.html")
		response = Client().get('/login/')
		self.assertTemplateUsed(response,"account/login.html")
	
	def test_create_user_with_email_and_username_successful(self):
		email = 'test@gmail.com'
		username = 'usernametest'
		password = 'thispasswordisbad'
		user = get_user_model().objects.create_user(
			email=email,
			username=username,
			password=password
			)
		
		self.assertEqual(user.email, email)
		self.assertTrue(user.check_password(password))
		
	def test_new_user_email_normalized_and_username(self):
		email = 'test@gmail.com'
		user = get_user_model().objects.create_user(email, 'test123','test123')
		self.assertEqual(user.email, email.lower())
		self.assertEqual(user.username, 'test123')
		
	def test_new_user_invalid_email(self):
		with self.assertRaises(ValueError):
			get_user_model().objects.create_user(None, 'test123', 'test123')

	def test_create_new_superuser(self):
		user = get_user_model().objects.create_superuser(
			'testadmin@gmail.com',
			'test123',
			'testadmin123'
			)
		self.assertTrue(user.is_superuser)
		self.assertTrue(user.is_staff)
	
	def test_user_can_login_on_url(self):
		email = 'test@gmail.com'
		username = 'usernametest'
		password = 'thispasswordisbad'
		user = get_user_model().objects.create_user(
			email=email,
			username=username,
			password=password
			)
		response = Client().post('/login/', data={
			"email" : "test@gmail.com", "password": "thispasswordisbad"})
		self.assertEqual(response.status_code, 302)
		
	def test_user_can_register_on_url_and_then_login(self):
		email = 'test@gmail.com'
		username = 'uname'
		password1 = 'te3S@tPass'
		response = self.client.post(reverse('register'), data={
			"email" : email,
			"username" : username,
			"password1" : password1,
			"password2" : password1,})
		self.assertEqual(response.status_code, 302)
		count = get_user_model().objects.all().count()
		self.assertEqual(count, 1)
		response = Client().post('/login/', data={
            "email" : email, "password": password1})
		self.assertEqual(response.status_code, 302)
	
	def test_logged_in_user_goes_to_login_again(self):
		email = 'test@gmail.com'
		username = 'usernametest'
		password = 'thispasswordisbad'
		user = get_user_model().objects.create_user(
			email=email,
			username=username,
			password=password
			)
		response = Client().post('/login/', data={
			"email" : "test@gmail.com", "password": "thispasswordisbad"})
		response = Client().get('/login/')
		self.assertEqual(response.status_code, 200)