from django.contrib import admin
from django.urls import path, include
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('tinymce', include('tinymce.urls')),
    path('admin/', admin.site.urls),
    path('', include('home.urls')),
    path('schedule/', include('Schedule.urls')),
    path('doctor/', include('doctor.urls', namespace='doctor')),
    path('article/', include('article.urls', namespace='article')),
	path('', include('account.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
